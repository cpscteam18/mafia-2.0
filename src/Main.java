
import java.util.List;
import java.util.Scanner;
public class Main {
	
	private static Scanner scan;
	

	// Mark up for System.out.println()
	static boolean[] ln = null;
	private static void $(String text, boolean... line){
		System.out.println(text);
		// If optional argument ln is present, a new line is printed
		if(line == null) System.out.println();
	}
	
	public static void main(String[] args) {
		
		Game game;
		int totalPlayers = 0;
		int lynchTarget = 0;
		String playTheme = null;
		List<Player> playerInfo = null;
		List<String> mafiaMembers = null;
		
		displayMenu();
		
		while (true) {
			scan = new Scanner(System.in);
			String action = scan.nextLine();
			switch(action) {
  			//Initialize the game class
  				case "N":
  					Players p = new Players();
  					p.init();
  					totalPlayers = p.getTotalPlayers();
  					lynchTarget = p.getLynchTarget();
  					playTheme = p.getPlayTheme();
  					playerInfo = p.getPlayerInfo();
  					mafiaMembers = p.getmafiaMembers();

  					game = new Game(totalPlayers, lynchTarget, playTheme, playerInfo, mafiaMembers); //Initialize new Game
  					game.run();
  					break;
  					
    		//Show Instructions	
  				case "I":
  					instructions();
  					displayMenu();
  					break;
  					
  			//Show About	
  				case "A":
  					about();
  					displayMenu();
  					break;
  			//Initialize game in test mode
  				case "T":
  					game = new Game(totalPlayers, lynchTarget, playTheme, playerInfo, mafiaMembers);
  					//game.test();
  					break;
  					
  				case "S":
  					testStory();
  					break;
  					
  				case "R":
  					testRoleAssignment();
  					break;
  					
  					
  			// Error message	
  				default:
  					$("Please enter N, I, A, or T...", ln);
  			}
		}
	}
  
  // Prompts user to enter anything before returning
  // To the main menu
	private static void instructions() {
		displayInstructions();
		$("Press enter to continue", ln);
		//String x = scan.nextLine();
	}
  
	private static void about() {
		displayAbout();
		$("Press enter to continue", ln);
		//String x = scan.nextLine();
	}
  //The display functions
	private static void displayMenu() {
		$("Mafia!");
		$("N - New Game");
		$("I - Instructions");
		$("A - About", ln);
		$("Tests:");
		$("T - Test Game Class (Entire game)");
		$("S - Test Story Class");
		$("R - Test RoleAssignment Class");
	}
  
	// About (from main screen)
	private static void displayAbout() {
  		$("Version 2.2");
  		$("Computer Science 233: Final Project", ln);
  	
  	
  		$("Team");
  		$("Christilyn Arjona:      Quality Controller, Java Developer");
  		$("Ronelle Bakima:             Content Writer, Java Developer");
  		$("Pierce De Jong:               Contact, Lead Java Developer");
  		$("Elvin Limpin:     User Experience Designer, Java Developer");
  		$("Mahsa Lofti Gaskarimahalleh: Code Reviewer, Java Developer ", ln);
  	
  	
  		$("Copyright 2017. All Rights Reserved.", ln);  
	}
  
	//TODO make instructions
	private static void displayInstructions() {
		$("Here are the instructions", ln);
	}
	
	private static void testStory(){
		$("Enter either alive or dead or main: ");
		String status = scan.next();
		while(status.equals("alive")||status.equals("dead")){
			String name = "Frodo";
			Story s = new Story(name);
			s.information();
			s.initialScenario();
			if(status.equals("dead")){
				s.dead();
			}else if (status.equals("alive")){
				s.healed();
			}
			$("Enter either alive or dead or main: ");
			status = scan.next();
		}
		main(null);
	}
	
	private static void testRoleAssignment(){
		$("Enter amount of players (5-10): ");
		int players = scan.nextInt();
		while(players>4 && players<11){
			
			RoleAssignment r = new RoleAssignment(players);
			r.playerAssignment();
			System.out.println(r.getRoles());
			System.out.println(r.getRolesInfo());
			System.out.println(r.getGoals());
			$("Enter amount of players (5-10): ");
			players = scan.nextInt();
		}
		main(null);
	}
}