//Author: Pierce de Jong 30006609

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**Game Class
	*This class runs the main logic and loop for the game
	*This class uses the Players class to assign values for the amount of players and their names vis Player Input
	*It then receives a list of all the Players with their assigned role etc.
	*The class then enters the game loop and asks for the user input on the first person to be lynched on Day 1 dayCycle()
	*The game then goes to the nightCycle() which asks for the target of each player every night. Players may die during this phase also
	*The gave is over when the Mafia out number the town members or no Mafia members are left
*/

public class Game extends Action {

	private  Scanner scan = new Scanner(System.in);;
	
	//This int stores the total number of PLayers, does not once entered in numberOfPlayers()
	private int totalPlayers;
	
	//Tracker for who is playing during the night cycle
	private int turn;
	
	//Runs the game loop until the game is over
	private boolean runTime;
	
	private String playTheme;
	
	//Boolean for if the Mafia or town people win at the end of the game
	private String whoWins;
	
	//String for the message the GodFather can give to Mafia members the next night *not implemented yet
	private String mafiaMessage;
	
	//List of all of the Mafia members to be presented to each one every night
	private List<String> mafiaMembers = new ArrayList<>();
	
	//List of each player (class) and his/her info (name, role, target, position, etc)
	private   List<Player> playerInfo = new ArrayList<>();
	
	
	//Index value for the target of the Lyncher
	private int lynchTarget;
	
	public Game(int totalPlayers, int lynchTarget, String playTheme, List<Player> playerInfo, List<String> mafiaMembers){
		super(playerInfo);
		this.totalPlayers = totalPlayers;
		this.lynchTarget = lynchTarget;
		this.playTheme = playTheme;
		this.playerInfo = playerInfo;
		this.mafiaMembers = mafiaMembers;
	}
	
	/**
	 *This method is the backbone of everything
	 *Calls the methods needed to collect all information about the players
	 *Runs a loop that calls the Day and Night cycles until the game is over
	 *After each night cycle, the Action class get called to work through all of the targets that each player selected
	 *The method 
	 */
	public void run(){
		
		initJobPositionMap();
		runTime = true;
		while(runTime){
			dayCycle();//This method handles voting and lynching a new person each day	
			nightCycle();//This method asks each player in order for the target each night
			nightActions();
			resetStatus();//Rests each player back to the alive status and removes anything that may have happened to them
			checkGameOver();
		}
		if(whoWins == "Mafia"){
			System.out.println("The Mafia have taken control of the town");
		}else if(whoWins == "Town"){
			System.out.println("The Mafia have all been killed");
		}else if(whoWins== "Lyncher"){
			System.out.println("The Lyncher has lynched his target and won");
		}
	}
	

    public void initJobPositionMap() {
        for (Player player : playerInfo) {
            playersJobPosition.put(player.getRole(), player.getPlayPosition());
        }
    }
	
	/**
	 * This method will kill one player each day, depending on who the players vote out.
	 */
	private void dayCycle(){
		System.out.println();
		System.out.println("*DAY TIME*");
		System.out.println("Each day 1 person must be lynched");
		System.out.println("The target of the lynching must have a majority (over 50%) vote to be lynched");
		//This prints out on a row, all the possible targets for each player to select.
		for(int i=0;i<totalPlayers;i++){
			//if the player is dead do not show them
			if (playerInfo.get(i).getStatus()!=4){
				System.out.print(playerInfo.get(i).getPlayPosition()+1+ ":" +playerInfo.get(i).getName()+" | ");
			}
		}
		System.out.println();
		System.out.print("Please enter the number of the player to be lynched: ");
		int target = scan.nextInt()-1;
		System.out.println();
		System.out.println(playerInfo.get(target).getName()+" has been lynched");
		
		playerInfo.get(target).setStatus(4);//Sets the target of the lynching to dead, So they can not be used or targeted again
		playerInfo.get(target).setWasLynched(true);
	}
	
	/**
	 * This method runs the cycle for selecting a target for each player every night
	 * If the status of the player is 4 ( dead) will skip them as both a player an a target for each player
	 */
	private void nightCycle(){
		turn = 0;
		System.out.println();
		System.out.println("*NIGHT TIME*");
		while(turn < totalPlayers){
			//if the player is dead  then they can not select a new target
			if (playerInfo.get(turn).getStatus()!=4){
				System.out.println();
				System.out.println("Name: " + playerInfo.get(turn).getName());
				System.out.println("Role: " + playerInfo.get(turn).getRole());
				System.out.println("Goal: " + playerInfo.get(turn).getGoal());
				System.out.println("What you can do: " + playerInfo.get(turn).getRoleInfo());
				
				//Displays the Mafia members to the other Mafia players
				if(playerInfo.get(turn).getRole().contains("Mafia")){
					System.out.println("Mafia Members: " + mafiaMembers);
				}
				
				//This prints out on a row of all the possible targets (number and name) for each player to select.
				for(int i=0;i<totalPlayers;i++){
					//if the player is dead can not be selected as a target
					if (playerInfo.get(i).getStatus()!=4){
						System.out.print(playerInfo.get(i).getPlayPosition()+1+ ":" +playerInfo.get(i).getName()+" | ");
					}
				}
				
				System.out.println();
				System.out.print("Please enter the number for the player you wish to target. Enter 0 to do nothing: ");
				//Sets the target of the player to the User Input
				playerInfo.get(turn).setPlayerTarget(scan.nextInt()-1);
				
				int target = playerInfo.get(turn).getPlayerTarget();
				
				//If the player enters an invalid entry, loops until valid entry is entered
				while(target<-1||target>=totalPlayers){
					System.out.print("Please enter a valid number: ");
					playerInfo.get(turn).setPlayerTarget(scan.nextInt()-1);
					target = playerInfo.get(turn).getPlayerTarget();
				}
				
				//The detective must receive his information on his turn as he checks if a person is part of the Mafia or not
				if(playerInfo.get(turn).getRole().contains("Detective")) detective(turn);
			}
		turn++;
		}
		System.out.println();
	}
	
	//TODO documentation
	//Initialize the methods for all of the Actions of each character that does something at night 
    public void nightActions(){
    	// If such players exist, their actions will be implemented
    	
    	if(totalPlayers>5) barman(); 
        if(totalPlayers>6) bodyguard();
        if(totalPlayers>8) godFather();
        killer("hitman");
        if(totalPlayers>9) killer("vigilante");
        doctor();
    }

	/**
	 * This method resets the status for all players that are alive to 1
	 * It also calls the printDeath(int player) method if the status of the player is 1 (Dead) 
	 * and also sets the status to 4 (Dead for more than one turn)
	 * If the player was saved (Doctor targeted them to save that night) then prints the player was saved last night
	 */
	private void resetStatus(){
		for(int i=0;i<totalPlayers;i++){
			
			//If the player is in Protected status puts them into Alive Status (0)
			if(playerInfo.get(i).getStatus()==3){
				
				playerInfo.get(i).setStatus(0);
				
			//If the player is in targeted by Mafia prints the death story and puts them into Dead status (4)
			}else if(playerInfo.get(i).getStatus()==1){
				printEvent(i,"dead");
				playerInfo.get(i).setStatus(4);
				
			//If the player was Saved, prints the saved story and sets them to Alive status(0)
			}else if(playerInfo.get(i).getStatus()==2){
				System.out.println("Saved");
				printEvent(i,"alive");
				playerInfo.get(i).setStatus(0);
			}
			
			playerInfo.get(i).setInBar(false);//Removes any  player that may have been in the bar out
			playerInfo.get(i).setPlayerTarget(-1);//Resets the target for each player
		}
	}
	/**
	 * This method prints the death message of any player that may have died during the night
	 * Then prints that they were either killed by the attacker or saved by the doctor
	 * @param player, status
	 */
	private void printEvent(int player, String status){
		String name = playerInfo.get(player).getName();
		Story s = new Story(name);
		s.information();
		s.initialScenario();
		if(status.equals("dead")){
			s.dead();
		}else if (status.equals("alive")){
			s.healed();
		}
	}
	/**
	 * This method counts the number of alive players for both the Mafia and town members
	 * If there is no Mafia members left, the game is over and the town members win
	 * If the Mafia has more players then the game is over and the Mafia wins
	 */
	private void checkGameOver(){
		// Game Over
		int mafiaTotal = 0;
		int townTotal = 0;
		//loops through all of the players and counts the total amount of Mafia and town people that are alive at the end of the round (Status 0)
		for (int i = 0; i < totalPlayers; i++) {
			if(playerInfo.get(i).getRole().contains("Mafia")&&playerInfo.get(i).getStatus()==0) {
				mafiaTotal += 1;
			}else if(playerInfo.get(i).getRole().contains("Town")&&playerInfo.get(i).getStatus()==0){
				townTotal +=1;
			}	
		}
		if(playerInfo.get(lynchTarget).wasLynched()){
			whoWins = "Lyncher";
			runTime = false;
		}else if(mafiaTotal==0){
			whoWins = "Town";
			runTime = false; //Stops the game if there are no Mafia members alive
		}else if(mafiaTotal > townTotal){
			whoWins = "Mafia";
			runTime = false;
		}
	}
	

	public List<Player> getPlayerInfo(){
		return playerInfo;
	}
	
	public String getPlayTheme(){
		return playTheme;
	}
}