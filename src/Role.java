/**
 * 2/28/17
 *
 */
public enum Role {

    TOWNIE              ("Townie", "Do nothing at night", "To have only town members left"),
    DETECTIVE           ("Detective", "Reveals the team for one player per night", "To have only town members left"),
    MAFIA_HITMAN        ("Mafia: Hitman", "May kill someone each night", "To make the majority of the town mafia members"),
    DOCTOR              ("Doctor", "May heal one player each night (Can't target same person two nights in a row)", "To have only town members left"),
    SURVIVOR            ("Survivor", "Do nothing at night", "To be the last town member left alive"),
    MAFIA_BARMAN        ("Mafia: Barman", "May stop the action of another player each night (Can't go same person each night)", "To make the majority of the town mafia members"),
    BODYGUARD           ("Bodyguard", "May save another person by stepping in front of the bullet per night. (You will die in their place)", "To have only town members left"),
    LYNCHER             ("Lyncher", "Do nothing at night", "To Lynch a specific player to win solo win the game | "),
    MAFIABOSS_GODFATHER ("Mafiaboss- GodFather", "Hidden from the Detective. Can send a message to another Mafia memeber each night", "To make the majority of the town mafia members"),
    VIGILANTE           ("Vigilante", "May kill new person each night, but is trying to kill Mafia. Note: Do not have to kill someone each night", "To have only town members left");

    private final String roleID;
    private final String roleInfo;
    private final String roleGoal;

    Role(String id, String role, String goal) {
        this.roleID = id;
        this.roleInfo = role;
        this.roleGoal = goal;
    }

    String getRoleID() {
        return roleID;
    }

    String getRoleInfo() {
        return roleInfo;
    }

    String getRoleGoal() {
        return roleGoal;
    }


}